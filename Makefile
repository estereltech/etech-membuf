#
# Makefile personnalisé utilisé avec Geany et/ou un terminal.
#

#Shell par défaut.
SHELL=/bin/sh

# Compilateur.
CC?=cc

# Nom de l'exécutable en sortie.
APP_NAME=membuf

# Le/les fichiers source à compiler.
SRCS=$(wildcard *.c)

# Fichier de sortie.
OUT=-o $(APP_NAME)

# Optimisé, sans information de débogage.
CRELEASE=-DNDEBUG -Ofast -s

# Débogage.
CDEBUG=-Og -g

# Options de compilation - C11 etc.
CFLAGS=-std=gnu11 -Wall -Wextra -Werror -march=native -D_GNU_SOURCE

# Liaisons, bibliothèques et inclusions.
CPPFLAGS=
LDFLAGS=

# Cible par défaut
.DEFAULT_GOAL=release


# Compilation.
release: $(SRCS)
	$(CC) $(CRELEASE) $(CFLAGS) $(OUT) $^

debug: $(SRCS)
	$(CC) $(CDEBUG) $(CFLAGS) $(OUT) $^

# Nettoyage.
clean:
	$(RM) *.h~
	$(RM) *.c~
	$(RM) Makefile~

clobber: clean
	$(RM) $(APP_NAME)
