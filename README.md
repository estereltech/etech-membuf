**Module C membuf[h,c]**  
Bloc mémoire de taille variable.  

*Pour construire le programme en mode optimisé :*  
$ make

*Pour construire le programme en mode de débogage :*  
$ make debug

*Pour exécuter le programme :*  
$ ./membuf
