#include "hp_timer.h"

#include <stdio.h>
#include <string.h>
#include <time.h>


//// Privé.


#define TIMER_LEN 0x10

struct sttimer
{
	struct timespec start_time;
	struct timespec end_time;
	long time_elapsed_nanos;

	char elapsed_nanos[TIMER_LEN];
	char elapsed_millis[TIMER_LEN];
	char elapsed_seconds[TIMER_LEN];
};


///// Public.


// Calcul du temps écoulé.
void
etech_timer_start(struct sttimer *hpt)
{
	hpt->time_elapsed_nanos = 0;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &hpt->start_time);
}

void
etech_timer_end(struct sttimer *hpt)
{
    clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &hpt->end_time);
    hpt->time_elapsed_nanos = hpt->end_time.tv_nsec - hpt->start_time.tv_nsec;
}

// Formatage pour printf.
const char*
etech_timer_print_nanos(struct sttimer *hpt)
{
	snprintf(hpt->elapsed_nanos, TIMER_LEN, "%ld", hpt->time_elapsed_nanos);

	return hpt->elapsed_nanos;
}

const char*
etech_timer_print_millis(struct sttimer *hpt)
{
	snprintf(hpt->elapsed_millis, TIMER_LEN, "%f",
										(float)hpt->time_elapsed_nanos/1000000);

	return hpt->elapsed_millis;
}

const char*
etech_timer_print_seconds(struct sttimer *hpt)
{
	snprintf(hpt->elapsed_seconds, TIMER_LEN, "%f",
									(float)hpt->time_elapsed_nanos/1000000000);

	return hpt->elapsed_seconds;
}

// Constructeur.
struct sttimer*
etech_timer_new(void)
{
	return (struct sttimer*)malloc(sizeof(struct sttimer));
}

// Destructeur.
void
etech_timer_free(struct sttimer *hpt)
{
	if ( hpt != NULL ) {
		free(hpt);
	}
	hpt = NULL;
}

