#pragma once


/*

	Projet		: Chronomètre de précision (nano seconde) en C.
	Auteur		: © Philippe Maréchal
	Date		: 24 janvier 2016
	Version		: 1.0

	Notes		:

	A faire		:

	Révisions	:
	15 juillet 2016 > améliorations diverses.

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#include <stdlib.h>


typedef struct sttimer *EtechTimer;


// Constructeur & destructeur.
EtechTimer etech_timer_new(void);
void etech_timer_free(EtechTimer);

// Début et fin du chronomètre.
void etech_timer_start(EtechTimer);
void etech_timer_end(EtechTimer);

// nanosecondes, millisecondes et secondes.
const char* etech_timer_print_nanos(EtechTimer);
const char* etech_timer_print_millis(EtechTimer);
const char* etech_timer_print_seconds(EtechTimer);

