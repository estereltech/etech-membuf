/*

	Projet		: MemBuffer : bloc mémoire de taille variable.
	Auteur		: © Philippe Maréchal
	Date		: 23 juin 2015

	Notes		:

	Révisions	:
	9 juillet 2015 | 14 juillet 2015

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#include "membuf.h"
#include "hp_timer.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <unistd.h>


#define __unused 	__attribute__((unused))
#define __used 		__attribute__((used))


int
main(int __unused argc, char  __unused **argv)
{
	printf("\nDébut du programme***********************************\n\n");

	// Chronomètre de précision.
	EtechTimer timer = etech_timer_new();
	if ( timer == NULL ) {
		printf("timer == NULL\n");

		return EXIT_FAILURE;
	}
	etech_timer_start(timer);

	// Bloc mémoire.
	printf("Premier bloc mémoire :\n");
	MemBuffer mbuf = buffer_new(128);
	if ( mbuf == NULL ) {
		printf("mbuf == NULL\n");

		return EXIT_FAILURE;
	}

	buffer_append_string(mbuf, "Gaston");
	buffer_append_char(mbuf, 0x20);
	buffer_append_string(mbuf, "Lagaffe");
	buffer_append_format_string(mbuf, " a %d ans.", 30);
	printf("%s\n", buffer_to_string(mbuf));

	printf("Avant buffer_shrink | Capacité = %zu | Taille = %zu\n",
		buffer_get_capacity(mbuf), buffer_get_size(mbuf));
	buffer_shrink(mbuf);
	printf("Après buffer_shrink | Capacité = %zu | Taille = %zu\n",
		buffer_get_capacity(mbuf), buffer_get_size(mbuf));

	printf("Recherche de \"30\" et copie dans un autre bloc :\n");
	size_t sPos = 0;
	const char search[] = "30";
	if ( buffer_find(mbuf, search, &sPos) ) {
		printf("sPos = %zu\n", sPos);
		MemBuffer extr = buffer_new(4);
		buffer_append_data(extr, buffer_get_mem(mbuf) + sPos, strlen(search));
		printf("%s\n", buffer_to_string(extr));
		buffer_free(extr);
	}
	printf("\n");

	// Second bloc mémoire.
	printf("Second bloc mémoire :\n");
	MemBuffer mbuf2 = buffer_new(128);
	buffer_append_string(mbuf2, "Mon");
	buffer_append_string(mbuf2, " chat");
	buffer_append_format_string(mbuf2, " a %d ans.", 4);
	printf("%s\n", buffer_to_string(mbuf2));
	printf("\n");

	printf("Avant buffer_shrink | Capacité = %zu | Taille = %zu\n",
		buffer_get_capacity(mbuf2), buffer_get_size(mbuf2));
	buffer_shrink(mbuf2);
	printf("Après buffer_shrink | Capacité = %zu | Taille = %zu\n",
		buffer_get_capacity(mbuf2), buffer_get_size(mbuf2));

	buffer_free(mbuf);
	printf("\n");

	// Copie d'une partie d'un autre bloc.
	printf("Copie d'une partie d'un bloc dans un autre :\n");
	MemBuffer mbuf3 = buffer_new(64);
	buffer_append_data(mbuf3, buffer_get_mem(mbuf2), 4);
	printf("%d - %s\n", __LINE__, buffer_to_string(mbuf3));
	buffer_free(mbuf2);
	buffer_free(mbuf3);
	printf("\n");

	// Chaînes littérales.
	printf("Chaînes littérales :\n");
	MemBuffer mbuf4 = buffer_new(128);
	char szLiteral[] = R"(Le nom de mon chat est "Misty")";
	buffer_append_string(mbuf4, szLiteral);
	printf("%s\n", buffer_to_string(mbuf4));
	printf("Capacité = %zu | Taille = %zu\n", buffer_get_capacity(mbuf4),
														buffer_get_size(mbuf4));

	char szLiteral2[] = R"( et mon prénom est "Philippe".)";
	buffer_inc_cap(mbuf4, 64);
	buffer_append_string(mbuf4, szLiteral2);
	printf("%s\n", buffer_to_string(mbuf4));
	printf("Ligne %d - Capacité = %zu | Taille = %zu\n", __LINE__,
						buffer_get_capacity(mbuf4), buffer_get_size(mbuf4));

	// buffer_reset.
	buffer_reset(mbuf4, 64);
	printf("Ligne %d - Capacité = %zu | Taille = %zu\n", __LINE__,
						buffer_get_capacity(mbuf4), buffer_get_size(mbuf4));
	buffer_append_string(mbuf4, "buffer_append_string");
	buffer_append_format_string_len(mbuf4, 64,
								" buffer_append_format_string_len %d", 64);
	printf("Ligne %d - Capacité = %zu | Taille = %zu\n", __LINE__,
						buffer_get_capacity(mbuf4), buffer_get_size(mbuf4));
	printf("%s\n", buffer_to_string(mbuf4));
	buffer_free(mbuf4);

	// Ajout à un bloc du contenu d'un autre.
	MemBuffer mbuf5 = buffer_new(64);
	buffer_append_string(mbuf5, "Bonjour je m'appelle ");
	printf("Capacité = %zu | Taille = %zu\n", buffer_get_capacity(mbuf5),
														buffer_get_size(mbuf5));

	MemBuffer mbuf6 = buffer_new(64);
	buffer_append_string(mbuf6, "Gaston Lagaff");
	printf("Capacité = %zu | Taille = %zu\n", buffer_get_capacity(mbuf6),
														buffer_get_size(mbuf6));
	buffer_append_data(mbuf5, buffer_get_mem(mbuf6), buffer_get_size(mbuf6));

	buffer_append_char(mbuf5, 'e');

	printf("Capacité = %zu | Taille = %zu\n", buffer_get_capacity(mbuf5),
														buffer_get_size(mbuf5));
	printf("%s\n", buffer_to_string(mbuf5));

	buffer_free(mbuf5);
	buffer_free(mbuf6);

	printf("\n\n");

	printf("Fin du programme*************************************\n\n");
	etech_timer_end(timer);
	printf("Temps écoulé (ns) %s\n", etech_timer_print_nanos(timer));
	printf("Temps écoulé (ms) %s\n", etech_timer_print_millis(timer));
	printf("Temps écoulé (s)  %s\n", etech_timer_print_seconds(timer));

	etech_timer_free(timer);

	return EXIT_SUCCESS;
}

