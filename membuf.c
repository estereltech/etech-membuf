#include "membuf.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <limits.h>


//// Privé.


struct stmembuf
{
	char 	*buffer;		// Pointeur vers le bloc mémoire.
	size_t 	size;			// Nombre d'octets de données effectives.
	size_t 	capacity;		// Capacité allouée.
};


// Une seule fonction pour l'allocation et les réallocations du bloc mémoire.
static bool
alloc(struct stmembuf *buf, size_t capacity)
{
	if ( capacity == 0 ) {
		return false;
	}

	/*
	 *  Si le bloc est déjà alloué, c'est une réallocation, s'il est NULL
	 * (initialisation) realloc équivaut à malloc.
	 */
	char *nptr = (char*)realloc(buf->buffer, capacity * sizeof(char));
	if ( NULL == nptr ) {
		return false;
	}

	buf->buffer = nptr;

	buf->capacity = capacity;

	return true;
}


//// Public.


// Réduire la capacité à la taille du contenu (shrink to fit).
bool
buffer_shrink(struct stmembuf *buf)
{
	return alloc(buf, buf->size);
}

// Réduire la capacité mais avec un zéro terminal.
bool
buffer_shrink0(struct stmembuf *buf)
{
	// Si le tableau est vide.
	if ( ! buf->buffer ) {
		return false;
	}

	// Réallocation éventuelle du tableau.
	if ( (buf->size + 1) > buf->capacity ) {
		alloc(buf, buf->capacity + 1);
	}

	buf->buffer[buf->size] = 0x00;

	buf->size += 1;

	return alloc(buf, buf->size);
}

// Augmenter la capacité.
bool
buffer_inc_cap(struct stmembuf *buf, size_t cap)
{
	return alloc(buf, buf->capacity + cap);
}

// Réduire la capacité.
bool
buffer_dec_cap(struct stmembuf *buf, size_t cap)
{
	// Pas de capacité négative.
	if ( cap > buf->capacity ) {
		cap = buf->capacity;
	}

	return alloc(buf, buf->capacity - cap);
}

// Ajout d'octets et réallocation éventuelle.
bool
buffer_append_data(struct stmembuf *buf, void *data, size_t size)
{
	if ( size == 0 ) {
		return false;
	}

	// Réallocation.
	if ( (buf->size + size) > buf->capacity ) {
		alloc(buf, buf->capacity + size);
	}

	// Ajout.
	if ( ! buf->buffer || buf->capacity == 0 ) {
		return false;
	}

	void *v = memcpy((void*)buf->buffer + buf->size, data, size);
	if ( NULL == v ) {
		return false;
	}

	buf->size += size;

	return true;
}

// Ajout d'une chaîne de caractères.
bool
buffer_append_string(struct stmembuf *buf, const char *str)
{
	return buffer_append_data(buf, (char*)str, strlen(str));
}

// Ajout au bloc mémoire d'une chaîne formatée avec indication d'une longueur.
bool
buffer_append_format_string_len(struct stmembuf *buf,
								size_t approx_len,
								const char *informat, ...)
{
	int ires = -1;

	// Allocation d'un bloc mémoire de sApproxLen octets.
	char *buffer = (char*)malloc(approx_len * sizeof(char));
	if ( NULL == buffer ) {
		return false;
	}

	va_list args;

	va_start(args, informat);
	ires = vsnprintf(buffer, approx_len, informat, args);
	va_end(args);

	// Erreur de formatage.
	if ( ires < 0 ) {
		free(buffer);
		buffer = NULL;

		return false;
	}

	bool bret = false;

	// Réussite.
	if ( (ires + 1) <= (int)approx_len ) {
		bret = buffer_append_string(buf, buffer);

		free(buffer);
		buffer = NULL;

		return bret;
	}

	// Affecter le nombre d'octets nécessaire avec une réallocation du bloc.
	approx_len = ires + 1;
	char *nptr = (char*)realloc(buffer, approx_len * sizeof(char));
	if ( NULL == nptr ) {
		free(buffer);
		
		return false;
	}
	buffer = nptr;

	// Nouvelle opération.
	va_start(args, informat);
	ires = vsnprintf(buffer, approx_len, informat, args);
	va_end(args);

	// Si cette fois le résultat est mauvais => échec ou exception.
	if ( ires > (int)approx_len ) {
		free(buffer);
		buffer = NULL;

		return false;
	}

	bret = buffer_append_string(buf, buffer);

	if ( NULL != buffer ) {
		free(buffer);
	}
	buffer = NULL;

	return bret;
}

// Fonction identique à la précédente commençant avec 256 octets.
bool
buffer_append_format_string(struct stmembuf *buf, const char *informat, ...)
{
	// 256 octets.
	size_t approx_len = UCHAR_MAX + 1;
	int ires = -1;

	// Allocation d'un bloc mémoire de 256 octets.
	char *buffer = (char*)malloc(approx_len * sizeof(char));
	if ( NULL == buffer ) {
		free(buffer);
		
		return false;
	}

	va_list args;

	va_start(args, informat);
	ires = vsnprintf(buffer, approx_len, informat, args);
	va_end(args);

	// Erreur de formatage.
	if ( ires < 0 ) {
		free(buffer);
		buffer = NULL;

		return false;
	}

	bool bret = false;

	// Si sApproxLen est suffisant => réussite.
	if ( (ires + 1) <= (int)approx_len ) {
		bret = buffer_append_string(buf, buffer);

		free(buffer);
		buffer = NULL;

		return bret;
	}

	// Sinon affecter le nombre d'octets nécessaire retourné par iRes.
	approx_len = ires + 1;
	char *nptr = (char*)realloc(buffer, approx_len * sizeof(char));
	if ( NULL == nptr ) {
		return false;
	}
	buffer = nptr;

	va_start(args, informat);
	ires = vsnprintf(buffer, approx_len, informat, args);
	va_end(args);

	// Si cette fois le résultat est mauvais => échec ou exception.
	if ( ires > (int)approx_len ) {
		free(buffer);
		buffer = NULL;

		return false;
	}

	bret = buffer_append_string(buf, buffer);

	if ( NULL != buffer ) {
		free(buffer);
	}
	buffer = NULL;

	return bret;
}

// Ajout d'un caractère et réallocation.
bool
buffer_append_char(struct stmembuf *buf, char c)
{
	return buffer_append_data(buf, &c, sizeof(c));
}

// Recherche à partir du début du tableau et renvoi en paramètre de la position.
bool
buffer_find(struct stmembuf *buf, const char *target, size_t *pos)
{
	if ( *target == 0x00 ){
		return false;
	}

	char *found = strstr(buf->buffer, target);

	if ( found ) {
		*pos = found - buf->buffer;

		return true;
	}

	return false;
}


// Recherche à partir de la fin du tableau.
bool
buffer_rfind(struct stmembuf *buf, const char *target, size_t *pos)
{
	if ( *target == 0x00 ) {
		return false;
	}

	const char *curr = buf->buffer + buf->size - strlen(target), *found = NULL;

	while (curr >= buf->buffer) {
		found = strstr(curr--, target);
		if ( found ) {
			*pos = (char*)found - buf->buffer;

			return true;
		}
	}

	return false;
}

// Tronquer (retirer des octets à la fin du tableau).
bool
buffer_shorten_right(struct stmembuf *buf, size_t minus, bool bcap)
{
	if ( minus == 0 ) {
		return false;
	}

	if ( bcap ) {
	
		// Réduction de la capacité.
		if ( ! alloc(buf, buf->size - minus) ) {
			return false;
		}
	} else {
		buf->buffer[buf->size - minus] = 0x00;
	}

	// Nouveau nombre d'octets.
	buf->size -= minus;

	return true;
}


// Etêter (retirer des octets au début du tableau).
bool
buffer_shorten_left(struct stmembuf *buf, size_t minus, bool bcap)
{
	if ( minus == 0 ) {
		return false;
	}

	// Nouveau bloc mémoire.
	char *new_buf = (char*)malloc((buf->size - minus) * sizeof(char));
	if ( NULL == new_buf ) {
		return false;
	}

	// Copie de la chaîne étêtée.
	void *v = memcpy(new_buf, buf->buffer + minus, (buf->size - minus));
	if ( NULL == v ) {
		free(new_buf);
		new_buf = NULL;

		return false;
	}

	// Suppression du bloc mémoire actuel.
	free(buf->buffer);
	buf->buffer = NULL;

	// Affectation du nouveau bloc mémoire.
	buf->buffer = new_buf;

	// Réallocation pour adapter la capacité.
	if ( bcap ) {
		if ( ! alloc(buf, buf->size - minus) ) {
			return false;
		}
	}

	buf->size -= minus;

	return true;
}

bool
buffer_reset(struct stmembuf *buf, size_t capacity)
{
	// Augmenter ou réduire la capacité.
	if ( buf->capacity < capacity ) {
		if ( ! buffer_inc_cap(buf, capacity - buf->capacity) ) {
			return false;
		}
	} else if ( buf->capacity > capacity ) {
		if ( ! buffer_dec_cap(buf, buf->capacity - capacity) ) {
			return false;
		}
	}

	// La taille du contenu est 0.
	buf->size = 0;

	return true;
}

/*
 * Ajoute un zéro terminal à la fin des données du bloc mémoire.
 * Note : le bloc est ici volontairement transformé en chaîne.
 */
char*
buffer_to_string(struct stmembuf *buf)
{
	/*
	 * Incrémenter la capacité si nécessaire afin de placer
	 * un zéro terminal à la fin du bloc mémoire.
	 */
	if ( buf->capacity < buf->size + 1 ) {
		buffer_inc_cap(buf, 1);
	}

	buf->buffer[buf->size] = 0x00;

	return buf->buffer;
}

/*
 * Crée une chaîne avec zéro terminal à partir des données du bloc mémoire et
 * la retourne. Elle doit être libére par free.
 * Note : le contenu bloc n'est pas ici transformé en chaîne.
 */
char*
buffer_as_string(struct stmembuf *buf)
{
	char *str = (char*)malloc((buf->size + 1) * sizeof(char));
	if ( NULL == str ) {
		return NULL;
	}

	void *m = memcpy(str, buf->buffer, buf->size);
	if ( NULL == m ) {
		free(str);

		return NULL;
	}
	
	str[buf->size] = 0x00;

	return str;
}

size_t
buffer_get_capacity(struct stmembuf *buf)
{
	return buf->capacity;
}

size_t
buffer_get_size(struct stmembuf *buf)
{
	return buf->size;
}

char*
buffer_get_mem(struct stmembuf *buf)
{
	return buf->buffer;
}

struct stmembuf*
buffer_new(size_t size)
{
	struct stmembuf *buf = NULL;
	if ( ( buf = (struct stmembuf*)malloc(sizeof(*buf)) ) != NULL ) {
		
		// Initialisations.
		buf->buffer 	= NULL;
		buf->size 		= 0;
		buf->capacity 	= 0;

		/*
		 * Si alloc est en erreur > libération de la mémoire allouée pour buf.
		 */
		if ( alloc(buf, size) ) {
			buf->capacity = size;
		} else {
			free(buf);
			buf = NULL;
		}
	}
	
	return buf;
}

// Libération des ressources allouées.
void
buffer_free(struct stmembuf *buf)
{
	if ( buf != NULL ) {
		free(buf->buffer);
		buf->buffer = NULL;

		free(buf);
		buf = NULL;
	}
}

