/*

	Projet		: MemBuffer > Bloc mémoire de taille variable.
	Auteur		: © Philippe Maréchal
	Date		: 30 avril 2013
	Version		: 1.6

	Notes		:
	Ce code a été développé à l'origine en C++ dans l'optique de recevoir des
	données (application/octet-stream) d'un serveur Web (protocole HTTP 1.1)
	Toujours prévoir une capacité suffisante afin d'éviter des réallocations
	répétées pour plus de performance.

	A faire		:

	Révisions 	:
	1er mai 2013 ( C++)
	23 juin 2015 > Refonte en utilisant malloc, realloc et free pour privilégier
	la performance sous Linux + Test unitaire effectué avec gdb et valgrind
	8 juillet 2015 > nouveaux tests sous VS 2013 et ajout des fonctions
	ZeroMem(), GetBufferAsUniquePtr(), AppendFormatString, AsString() et
	MakePart.
	14 juillet 2015 > meilleure implémentation de AppendFormatString en deux
	fonctions + ZeroLen()
	03 janvier 2016 > Portage en langage C > struct MemBuffer & version 1.3.
	04 janvier 2016 > une seule fonction d'allocation + suppression de copy_data.
	07 février 2016 > refonte partielle avec la structure en implémentation &
	version 1.4.
	16 mars 2016 > fonction buffer_shrink0.
	23 juin 2016 > fonction buffer_reset et version 1.5.
	17 août 2016 > modifications mineures dans 'buffer_free'.
	21 janvier 2017 > buffer_as_string devient buffer_to_string + nouvelle
	fonction buffer_as_string & version 1.6.
	16 juillet 2017 > correction du bogue dans 'buffer_free' & modifications
	dans 'buffer_new'.
	22 juillet 2017 > nouvelle modification dans 'buffer_new'.
	30 mars 2018 > meilleure implémentation de 'buffer_append_char'.
	08 décembre 2018 > modifications mineures dans l'emploi de realloc.
	02 mai 2019 > meilleurs commentaires pour 'GitLab/FramaGit'.

*/

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */


#pragma once

#include <stdlib.h>
#include <stdbool.h>


typedef struct stmembuf *MemBuffer;


// Constructeur & destructeur.
MemBuffer buffer_new(size_t size);
void buffer_free(MemBuffer buf);

// Opérations sur le bloc : ajout de caractère, données brutes et chaîne.
bool buffer_append_char(MemBuffer buf, const char c);
bool buffer_append_data(MemBuffer buf, void *data, size_t size);
bool buffer_append_string(MemBuffer buf, const char *str);

// Ajout d'une chaîne à formater avec indication de la taille approximative.
bool buffer_append_format_string_len(MemBuffer buf, size_t sApproxLen,
												const char *informat, ...);
// Ajout d'une chaîne à formater sans indication de la taille approximative.
bool buffer_append_format_string(MemBuffer buf, const char *informat, ...);

// Recherche d'une sous chaîne dans le bloc (à partir du début et de la fin).
bool buffer_find(MemBuffer buf, const char *target, size_t *pos);
bool buffer_rfind(MemBuffer buf, const char *target, size_t *pos);

// Etêtage et tronquage.
bool buffer_shorten_right(MemBuffer buf, size_t minus, bool bcap);
bool buffer_shorten_left(MemBuffer buf, size_t minus, bool bcap);

// Modification de la capacité du bloc.
bool buffer_inc_cap(MemBuffer buf, size_t size);
bool buffer_dec_cap(MemBuffer buf, size_t size);

// Réduction de la capacité à la taille du contenu.
bool buffer_shrink(MemBuffer buf);
// Réduction de la capacité à la taille du contenu avec zéro terminal.
bool buffer_shrink0(MemBuffer buf);

// Réinitialisation avec nouvelle capacité.
bool buffer_reset(MemBuffer buf, size_t capacity);

// Accesseurs : taille, capacité, bloc mémoire, chaîne en résultat.
size_t buffer_get_capacity(MemBuffer buf);
size_t buffer_get_size(MemBuffer buf);
char* buffer_get_mem(MemBuffer buf);
char* buffer_to_string(MemBuffer buf);
char* buffer_as_string(MemBuffer buf);

